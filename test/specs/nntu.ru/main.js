describe('Сайт NNTU.RU', () => {
    it('Сайт должен иметь корректный заголовок', () => {
        browser.maximizeWindow();
        browser.url('/');
        const title = browser.getTitle();
        const search = $("/html/body/div[@class='page']/div[@class='page-header']/div[@class='topbar']/div[@class='container']/div[@class='topbar-inner']/div[@class='topbar-right']/div[@class='topbar-serv']/div[@class='b-search']/form/button[@class='btn btn-primary-1 btn-search']");
        browser.takeScreenshot();
        expect(browser).toHaveTitle('НГТУ им. Р.Е. Алексеева');
    });

    it('Сайт должен иметь корректный заголовок 2', () => {
        browser.maximizeWindow();
        browser.url('/');
        expect(browser).toHaveTitle('НГТУ им. Р.Е. Алексеева');
    });
});
