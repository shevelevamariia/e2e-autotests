const CHROME_ARGS = [
    '--disable-background-networking',
    '--enable-features=NetworkService,NetworkServiceInProcess',
    '--disable-background-timer-throttling',
    '--disable-backgrounding-occluded-windows',
    '--disable-breakpad',
    '--disable-client-side-phishing-detection',
    '--disable-component-extensions-with-background-pages',
    '--disable-default-apps',
    '--disable-dev-shm-usage',
    '--disable-extensions',
    '--disable-features=TranslateUI,BlinkGenPropertyTrees',
    '--disable-hang-monitor',
    '--disable-ipc-flooding-protection',
    '--disable-popup-blocking',
    '--disable-prompt-on-repost',
    '--disable-renderer-backgrounding',
    '--disable-sync',
    '--force-color-profile=srgb',
    '--metrics-recording-only',
    '--no-first-run',
    '--enable-automation',
    '--password-store=basic',
    '--use-mock-keychain',
];

if (process.env.CHROME_HEADLESS === 'true') {
    CHROME_ARGS.push('--headless', '--no-sandbox', '--window-size=1920,1080');
}

exports.config = {
    runner: 'local',
    services: [
        ['selenium-standalone', {
            logPath: 'test/logs',
        }],
    ],
    seleniumLogs: 'test/logs',
    specs: [
        './test/specs/**/*.js',
    ],
    baseUrl: 'https://nntu.ru/',
    waitforTimeout: 10000,
    maxInstances: 5,
    capabilities: [{
        'browserName': 'chrome',
        'goog:chromeOptions': {
            args: CHROME_ARGS,
        }
    }],
    framework: 'mocha',
    reporters: [['allure', {
        outputDir: 'allure-results',
        disableWebdriverStepsReporting: true,
        disableWebdriverScreenshotsReporting: false,
        disableMochaHooks: true,
    }]],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000,
    },
    logLevel: 'info',
    outputDir: 'test/logs',
}
